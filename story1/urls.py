from . import views
from django.urls import path

app_name='story1'

urlpatterns = [
	path('', views.story1page, name='story1page')
]