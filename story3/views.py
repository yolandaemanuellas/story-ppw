from django.shortcuts import render

# Create your views here.
def story3page(request):
	response = {
		'title': 'Story 3'
	}
	return render(request, 'story3.html', response)

def gallery(request):
	response = {
		'title': 'Gallery'
	}
	return render(request, 'gallery.html', response)