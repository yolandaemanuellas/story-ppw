from django import forms
from django.forms import ModelForm 
from . import models

class CreateCoursesForm(forms.ModelForm):
	class Meta:
		model = models.Courses
		fields = [
			"name",
			"lecturer",
			"sks",
			"description",
			"period",
			"classroom",
		]

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		for field in self.Meta.fields:
			self.fields[field].widget.attrs.update({
				'class': 'form-control',
			})
