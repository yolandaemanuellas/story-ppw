from django.shortcuts import render

# Create your views here.
def story1page(request):
	response = {
		'title': 'Story 1'
	}
	return render(request, 'story1.html', response)