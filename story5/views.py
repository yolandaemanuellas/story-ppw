from django.shortcuts import render, redirect
from .models import Courses
from . import forms
# Create your views here.

def courses(request):
	courses = Courses.objects.all()
	response = {
		'courses' : courses
	}
	return render(request, 'courses.html', response)

def add_course(request):
    response = {
        'course_form': forms.CreateCoursesForm()
    }

    if request.method == "POST":
    	course_form = forms.CreateCoursesForm(request.POST)
    	if course_form.is_valid():
    		instance = course_form.save(commit=False)
    		instance.save()
    		return redirect('story5:courses')
    return render(request, 'add_course.html', response)

def delete_course(request, id):
	course = Courses.objects.get(id=id)
	course.delete()
	return redirect('story5:courses')

def course_detail(request, id):
    course = Courses.objects.get(id=id)
    response = {
        'course_content': course
    }
    return render(request, 'course_detail.html', response)
