from . import views
from django.urls import path

app_name = 'story5'

urlpatterns = [
    path('', views.courses, name='courses'),
    path('add_course', views.add_course, name='add_course'),
    path('delete_course/<int:id>', views.delete_course, name='delete_course'),
    path('course_detail/<int:id>', views.course_detail, name='course_detail'),
]
