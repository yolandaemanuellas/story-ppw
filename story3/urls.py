from . import views
from django.urls import path

app_name='story3'

urlpatterns = [
	path('', views.story3page, name='story3page'),
	path('gallery/', views.gallery, name='gallery'),
]