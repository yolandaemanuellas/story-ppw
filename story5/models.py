from django.db import models

# Create your models here.

class Courses(models.Model):
	name = models.CharField(max_length=100)
	lecturer = models.CharField(max_length=100)
	sks = models.IntegerField()
	description = models.TextField()
	period = models.CharField(max_length=100)
	classroom = models.CharField(max_length=100)

	def __str__(self):
		return "{} - {} - {} - {} - {} - {}".format(self.name, self.lecturer, self.sks, self.description, self.period, self.classroom)
